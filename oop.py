import utils

#https://www.youtube.com/watch?v=RT-Uu1yWRcc
class SportTeam:
    number_of_teams = 0
    fine_amount = 50_000

    def __init__(self, name, wins, losses):
        self.name = name
        self.wins = wins
        self.losses = losses
        self.total_fines = 0
        #incrémentation à chaque nouvelle instance
        SportTeam.number_of_teams += 1
        self.__class__.number_of_teams += 1

    @classmethod
    def from_string(cls, stats_as_string):
        name, wins, losses = stats_as_string.split('-')
        return cls(name, int(wins), int(losses))

    @classmethod
    def from_file(cls, stats_as_string):
        try:
            with open(stats_as_string) as f:
                # data = f.readline().strip().split('-')
                # return cls(data[0],int(data[1]),int(data[2]))
                #même chose pour éviter la duplication
                return cls.from_string(f.readline().strip())
        except:
            raise FileNotFoundError(f'file "{stats_as_string}" not found') from None


    @staticmethod
    def pluralize(total, singular, plural=None):
        return utils.pluralize(total, singular, plural)

    def get_fined(self):
        self.total_fines  += self.fine_amount

    def stats(self):
        return f"{self.name} has {self.pluralize(self.wins, 'win')} - {self.pluralize(self.losses, 'loss', 'losses')}"

    @classmethod
    def set_fine_amount(cls, amount):
        cls.fine_amount = amount

class BasketballTeam(SportTeam):
    number_of_teams = 0
    def stats(self):
        return '[BASKETBALL] STATS: ' + super().stats()

class FootballTeam(SportTeam):
    def __init__(self, name, wins, losses, draws=0):
        #à utiliser pour héritage simple
        super().__init__(name, wins, losses)
        self.draws = draws
        #même chose a tuliser pour héritage multiple
        #SportTeam.__init__(self, name, wins, losses)
    

    @classmethod
    def from_string(cls, stats_as_string):
        name, wins, losses, draws = stats_as_string.split('-')
        return cls(name, int(wins), int(losses))


    number_of_teams = 0
    def stats(self):
        return f"[FOOTBALL] STATS: {self.name} has {self.pluralize(self.wins, 'win')} - {self.pluralize(self.losses, 'loss', 'losses')} - {self.pluralize(self.draws, 'draw')}"

class BaseballTeam(SportTeam):
    number_of_teams = 0
    def stats(self):
        return '[BASEBALL] STATS: ' + super().stats()

print(f'nb d\'equipe {BasketballTeam.number_of_teams}')

team_1 = BasketballTeam('GoldenState', 0, 39)
team_2 = BasketballTeam('Los Angelas lakers',40, 1)

BasketballTeam.set_fine_amount(75_000)

print(team_1.fine_amount)
print(team_2.fine_amount)

team_string_1 = 'Nets-1-99'

team_3 = BasketballTeam.from_string(team_string_1)
team_4 = BasketballTeam.from_file('milwaukee.txt')

print(team_1.stats())
print(team_2.stats())
print(team_3.stats())
print(team_4.stats())

print(f'nb d\'equipe {BasketballTeam.number_of_teams}')

print(f'nb d\'equipe {FootballTeam.number_of_teams}')

team_5 = FootballTeam('OM', 3, 39, 10)
team_6 = FootballTeam('PSG',15, 1, 11)

FootballTeam.set_fine_amount(10_000)

print(team_5.fine_amount)
print(team_6.fine_amount)

team_string_2 = 'Lille-1-3-9'

team_7 = FootballTeam.from_string(team_string_2)
team_8 = FootballTeam.from_file('marseilles.txt')

print(team_5.stats())
print(team_6.stats())
print(team_7.stats())
print(team_8.stats())

print(f'nb d\'equipe {FootballTeam.number_of_teams}')

print(f'nb d\'equipe {BaseballTeam.number_of_teams}')

team_9 = BaseballTeam("Roger", 99, 1)
print(team_9.stats())

print(f'nb d\'equipe SportTeam {SportTeam.number_of_teams}')
print(f'nb d\'equipe BasketballTeam {BasketballTeam.number_of_teams}')
print(f'nb d\'equipe FootballTeam {FootballTeam.number_of_teams}')
print(f'nb d\'equipe BaseballTeam {BaseballTeam.number_of_teams}')

print(f'team_9.fine_amount {team_9.fine_amount}')
BaseballTeam.set_fine_amount(10_000)
team_9.get_fined()
print(f'team_9.fine_amount {team_9.fine_amount}')
print(f'team_1.fine_amount {team_1.fine_amount}')

team_10 = FootballTeam.from_file('no_exist')
print(team_10.stats())
