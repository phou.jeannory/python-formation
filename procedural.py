def pluralize(total, singular, plural=None):
    assert isinstance(total, int) and total >= 0, 'le total doit être une valeure positive'

    if not plural:
        plural = singular + 's'

    string = singular if total <= 1 else plural

    return f'{total} {string}'

def get_basketball_team_stats(team_name, wins, losses):
    return '[BASKETBALL] STATS: {}: {} - {}.'.format(team_name, pluralize(wins, 'victoire'), pluralize(losses, 'défaite'))

def get_football_team_stats(team_name, wins, losses):
    return f"[FOOTBALL] STATS: {team_name}: {pluralize(wins, 'victoire')} - {pluralize(losses, 'défaite')}"

# print(str(pluralize(10, 'phoque', 'cornichons')))
# print(get_basketball_team_stats('Milwalkee', 50, 15))
# print(get_football_team_stats('OM', 50, 1999))

# raptors_stats = 'Toronto Raptor-36-14'

# data = raptors_stats.split('-')
# print(data[0])
# print(data[1])
# print(data[2])

# print(get_basketball_team_stats(data[0],int(data[1]),int(data[2])))

with open('milwaukee.txt') as file:
    data = file.readline().strip().split('-')
    print(get_basketball_team_stats(data[0],int(data[1]),int(data[2])))

with open('marseilles.txt') as file:
    data = file.readline().strip().split('-')
    print(get_football_team_stats(data[0],int(data[1]),int(data[2])))
